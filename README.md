# Duran Duran Networks Theme

A custom theme for [Duran Duran Networks](https://duran-duran.net/).

## Dependencies

* GruntJS

## Implementations

### Movable Type ID Mapper

`Setup::register_mt_id_patterns()` registers patterns to match content containing Movable Type URls
with entry IDs. See the
[plugin documentation](https://bitbucket.org/NemesisVex/movable-type-id-mapper-for-wordpress)
for usage.

### Bootstrap Overrides for WordPress

`TemplateTags::paging_nav()` checks whether the Bootstrap Overrides plugin is enabled. If so,
it uses `Setup::wp_page_menu_args()` to pass arguments to the overridden paging functions.
See the [plugin documentation](https://bitbucket.org/NemesisVex/bootstrap-overrides-for-wordpress)
for usage.
